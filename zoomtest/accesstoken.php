<?php
require_once('zoomconfig.php');
// Zoom API OAuth Configuration
$clientId = ZOOM_CLIENT_ID;
$clientSecret = ZOOM_CLIENT_SECRET;
$redirectUri = ZOOM_CALLBACK;

// Step 1: Redirect the user to Zoom for authorization
if (!isset($_GET['code'])) {
    $authorizationUrl = 'https://zoom.us/oauth/authorize';
    $authorizationUrl .= '?response_type=code';
    $authorizationUrl .= '&client_id=' . $clientId;
    $authorizationUrl .= '&redirect_uri=' . urlencode($redirectUri);
    header('Location: ' . $authorizationUrl);
    exit;
}

// Step 2: Handle the callback from Zoom
if (isset($_GET['code'])) {
    $code = $_GET['code'];

    // Step 3: Exchange the authorization code for an access token
    $tokenUrl = 'https://zoom.us/oauth/token';
    $postData = array(
        'grant_type' => 'authorization_code',
        'code' => $code,
        'redirect_uri' => $redirectUri,
    );

    $options = array(
        'http' => array(
            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
            'method' => 'POST',
            'content' => http_build_query($postData),
        ),
    );

    $context = stream_context_create($options);
    $response = file_get_contents($tokenUrl, false, $context);

    if ($response === false) {
        die('Failed to retrieve access token');
    }

    $json = json_decode($response);

    if ($json === null || !isset($json->access_token)) {
        die('Failed to decode JSON response or access_token not found');
    }

    // You now have the access token
    $accessToken = $json->access_token;

    // You can use this access token to make API requests on behalf of the user
    echo 'Access Token: ' . $accessToken;
}
