<?php
require_once('dbconn.php');
$sql = "SELECT accesstoken FROM zoomapi WHERE isActive=1 and created_datetime > DATE(NOW()) order by desc limit 1;";
$result = $conn->query($sql);

if ($result !== false && $result->num_rows > 0) {

    while ($row = $result->fetch_assoc()) {
        define('ZOOM_AUTHORIZATION_CODE',  $row["accesstoken"]);        
    }
} else {
    
    header('Location: callback.php');
    // define('ZOOM_AUTHORIZATION_CODE', 'eyJzdiI6IjAwMDAwMSIsImFsZyI6IkhTNTEyIiwidiI6IjIuMCIsImtpZCI6ImQxODMzNzM0LTRlZjAtNDNmZi04M2FjLTM5Yzc3ZTg1YzQzZCJ9.eyJhdWQiOiJodHRwczovL29hdXRoLnpvb20udXMiLCJ1aWQiOiJrdFlsc3B4TlN4Q2ljZ21Qck93TllnIiwidmVyIjo5LCJhdWlkIjoiNjQ3NGEwMmFmNDk5MmM0ZmY4NjU2MDhlOTBjZGY5ZDUiLCJuYmYiOjE2OTk0NDc5MTIsImlzcyI6InptOmNpZDpvNmszeFEzblEwV3FXVm41Y1FlNU5BIiwiZ25vIjowLCJleHAiOjE2OTk0NTE1MTIsInR5cGUiOjIsImlhdCI6MTY5OTQ0NzkxMn0.sol3vz98qmJAzhNAlKCroSKhqT28Ty0T59jpqqeAA0u3eyqKrWR7eVNyuXBy2ADBW1Qf0qSL5oj0mAM6RNI2Zw');
    
}
$conn->close();
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://api.zoom.us/v2/users/me/meetings',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => '{
  "topic": "test",
  "type":2,
  "start_time": "2023-11-08T12:10:10Z",
  "duration":"3",
  "settings":{
   "host_video":true,
   "participant_video":true,
   "join_before_host":true,
   "mute_upon_entry":"true",
   "watermark": "true",
   "audio": "voip",
   "auto_recording": "cloud"
     } 
  
 }',
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Authorization: Bearer '.ZOOM_AUTHORIZATION_CODE,

    ),
));

$response = curl_exec($curl);

curl_close($curl);

$result = json_decode($response);

echo "<pre>";
print_r ($result);
echo "</pre>";

?>
Meeting Link: <a href="<?= $result->join_url ?>">Join</a>