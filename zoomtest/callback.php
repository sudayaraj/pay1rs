<?php
require_once('zoomconfig.php');
require_once('dbconn.php');

$clientId = ZOOM_CLIENT_ID;
$clientSecret = ZOOM_CLIENT_SECRET;


$tokenUrl = 'https://zoom.us/oauth/token';


$scope = 'meeting:write'; 
$grantType = 'client_credentials';


$data = array(
    'grant_type' => $grantType,
    'scope' => $scope,
);


$options = array(
    'http' => array(
        'header' => "Content-type: application/x-www-form-urlencoded\r\n"."Authorization: Basic " . base64_encode("$clientId:$clientSecret"),
        'method' => 'POST',
        'content' => http_build_query($data),
    ),
);

$context = stream_context_create($options);
$response = file_get_contents($tokenUrl, false, $context);

if ($response === false) {
    die('Failed to retrieve access token');
}

$json = json_decode($response);

if ($json === null || !isset($json->access_token)) {
    die('Failed to decode JSON response or access_token not found');
}


$accessToken = $json->access_token;


// echo 'Access Token: ' . $accessToken;


$sql = "INSERT INTO zoomapi (accesstoken) VALUES ('" . $accessToken . "');";
$result = $conn->query($sql);

$conn->close();
if ($result == true) {
    define('ZOOM_AUTHORIZATION_CODE',  $accessToken);  

    
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://api.zoom.us/v2/users/me/meetings',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => '{
  "topic": "test",
  "type":2,
  "start_time": "2023-11-08T12:10:10Z",
  "duration":"3",
  "settings":{
   "host_video":true,
   "participant_video":true,
   "join_before_host":true,
   "mute_upon_entry":"true",
   "watermark": "true",
   "audio": "voip",
   "auto_recording": "cloud"
     } 
  
 }',
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Authorization: Bearer '.ZOOM_AUTHORIZATION_CODE,

    ),
));

$response = curl_exec($curl);

curl_close($curl);

$result = json_decode($response);

echo "<pre>";
print_r ($result);
echo "</pre>";

?>
Meeting Link: <a href="<?= $result->join_url ?>">Join</a>
<?php
}
?>