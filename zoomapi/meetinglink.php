<?php
error_reporting(E_ALL); error_reporting(-1); ini_set('error_reporting', E_ALL);
include_once('zoomconfig.php');
// include_once('config.php');

$clientId = ZOOM_CLIENT_ID;
$clientSecret = ZOOM_CLIENT_SECRET;


$redirectUri = ZOOM_CALLBACK;

$zoomApiBaseUrl = 'https://api.zoom.us/v2';
$zoomCreateMeetingEndpoint = '/users/me/meetings';


$data = array(
    'topic' => 'Test Meeting',
    'type' => 2, 
    'start_time' => '2023-11-04T14:00:00Z', 
    'duration' => 60, 
);



$accessToken = ZOOM_AUTHORIZATION_CODE; 


$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $zoomApiBaseUrl . $zoomCreateMeetingEndpoint);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: Bearer ' . $accessToken,
    'Content-Type: application/json',
));
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

$response = curl_exec($ch);

if (curl_errno($ch)) {
    echo 'Error: ' . curl_error($ch);
} else {
    $responseData = json_decode($response, true);

    if (isset($responseData['join_url'])) {
        echo 'Meeting link: <a href="' . $responseData['join_url'].'"> Join Now </a>';
    } else {
        echo 'Error creating meeting: ' . $responseData['message'];
    }
}

curl_close($ch);
?>
