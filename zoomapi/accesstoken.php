<?php
error_reporting(E_ALL); error_reporting(-1); ini_set('error_reporting', E_ALL);
require 'zoomconfig.php';
require 'vendor/autoload.php'; // Include the OAuth2 client library

use League\OAuth2\Client\Provider\GenericProvider;

// Replace with your Zoom OAuth app credentials
$clientId = ZOOM_CLIENT_ID;
$clientSecret = ZOOM_CLIENT_SECRET;
$redirectUri = ZOOM_CALLBACK;

$provider = new GenericProvider([
    'clientId' => $clientId,
    'clientSecret' => $clientSecret,
    'redirectUri' => $redirectUri,
    'urlAuthorize' => 'https://zoom.us/oauth/authorize',
    'urlAccessToken' => 'https://zoom.us/oauth/token',
    'urlResourceOwnerDetails' => 'https://api.zoom.us/v2/users/me', // Adjust based on Zoom's API
    'scopes' => 'meeting:read meeting:write offline_access', // Specify your required scopes
]);

// Initiate the OAuth flow
$authorizationUrl = $provider->getAuthorizationUrl();
header('Location: ' . $authorizationUrl);
