<?php
require 'vendor/autoload.php'; // Include the Guzzle HTTP client library.

// Replace with your Zoom OAuth app's client ID and client secret.
$client_id = 'o6k3xQ3nQ0WqWVn5cQe5NA';
$client_secret = 'Br9CGUddhYT53m5q0zZMRNRZ6nlivyVM';

// Define the required parameters.
$token_url = 'https://zoom.us/oauth/token';
$meeting_url = 'https://api.zoom.us/v2/users/{me}/meetings'; // Replace {USER_ID} with the user's ID or 'me' for the current user.
$redirect_uri = 'https://example.com'; // This can be any valid URL.

// Define the payload for obtaining the access token.
$token_data = [
    'grant_type' => 'client_credentials',
    'client_id' => $client_id,
    'client_secret' => $client_secret,
];

// Initialize Guzzle HTTP client.
$client = new \GuzzleHttp\Client();

// Step 1: Request an access token using client credentials grant.
$response = $client->post($token_url, [
    'form_params' => $token_data,
]);

// Parse the response to get the access token.
$response_data = json_decode($response->getBody(), true);
$access_token = $response_data['access_token'];

// Step 2: Create a Zoom meeting using the access token.
$meeting_data = [
    'topic' => 'My Meeting',
    'type' => 2, // 2 for scheduled meeting
    'start_time' => '2023-11-03T14:00:00',
    'duration' => 60,
];

$response = $client->post($meeting_url, [
    'headers' => [
        'Authorization' => 'Bearer ' . $access_token,
    ],
    'json' => $meeting_data,
]);

// Parse the response to get the meeting link.
$meeting_info = json_decode($response->getBody(), true);

// Print the meeting link.
echo 'Meeting Link: ' . $meeting_info['join_url'];
