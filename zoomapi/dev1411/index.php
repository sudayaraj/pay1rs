<?php
include_once('zoomconfig.php');

$clientID = ZOOM_CLIENT_ID;
$clientSecret = ZOOM_CLIENT_SECRET;
$redirectURI = ZOOM_CALLBACK; 

$authUrl = 'https://zoom.us/oauth/authorize?response_type=code&client_id=' . $clientID . '&redirect_uri=' . urlencode($redirectURI);


header('Location: ' . $authUrl);
exit;

?>