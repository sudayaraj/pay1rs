<?php
include_once('zoomconfig.php');
$clientID = ZOOM_CLIENT_ID;
$clientSecret = ZOOM_CLIENT_SECRET;
$redirectURI = ZOOM_CALLBACK;

if (isset($_GET['code'])) {
   
    $authorizationCode = $_GET['code'];
  
    $tokenUrl = 'https://zoom.us/oauth/token';
    $tokenData = array(
        'grant_type' => 'authorization_code',
        'code' => $authorizationCode,
        'redirect_uri' => $redirectURI,
        'client_id' => $clientID,
        'client_secret' => $clientSecret,
    );

    $ch = curl_init($tokenUrl);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($tokenData));

    $response = curl_exec($ch);
    curl_close($ch);

   
    if ($response) {
        $tokenInfo = json_decode($response, true);
        $accessToken = $tokenInfo['access_token'];

        
        $baseUrl = 'https://api.zoom.us/v2/';
        $createMeetingUrl = $baseUrl . 'users/me/meetings';
        $meetingData = array(
            'topic' => 'Your Meeting Topic',
            'type' => 2, 
            'start_time' => '2023-11-14T12:00:00', 
        );

        $ch = curl_init($createMeetingUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $accessToken,
        ));

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($meetingData));

        $meetingResponse = curl_exec($ch);
        curl_close($ch);
        
        if ($meetingResponse) {
            $meetingInfo = json_decode($meetingResponse, true);            
            
            echo 'Meeting created successfully. Join URL: <a href="' . $meetingInfo['join_url'] . '" >' . $meetingInfo['join_url'].'</a>';
        } else {
            echo 'Error creating meeting.';
        }
    } else {
        echo 'Error obtaining access token.';
    }
} else {
    echo 'Authorization code not present in the URL.';
}
