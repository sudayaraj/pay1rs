<?php
$zoomClientId = 'o6k3xQ3nQ0WqWVn5cQe5NA'; // Replace with your Zoom App's client ID
$zoomClientSecret = 'Br9CGUddhYT53m5q0zZMRNRZ6nlivyVM'; // Replace with your Zoom App's client secret
$redirectUrl = 'https://pay1rs.com/welcome/callback'; // Replace with your actual callback URL

if (isset($_GET['code'])) {
    // The user has authorized your app, and Zoom has redirected back with an authorization code.
    $authorizationCode = $_GET['code'];

    // Exchange the authorization code for an access token.
    $tokenUrl = 'https://zoom.us/oauth/token';
    $data = array(
        'grant_type' => 'authorization_code',
        'code' => $authorizationCode,
        'redirect_uri' => $redirectUrl
    );

    $options = array(
        'http' => array(
            'header' => "Authorization: Basic " . base64_encode("$zoomClientId:$zoomClientSecret"),
            'method' => 'POST',
            'content' => http_build_query($data)
        )
    );

    $context = stream_context_create($options);
    $response = file_get_contents($tokenUrl, false, $context);

    if ($response === false) {
        die('Failed to retrieve access token');
    }

    $tokenData = json_decode($response, true);

    if (isset($tokenData['access_token'])) {
        $accessToken = $tokenData['access_token'];

        // You can now use the access token to make Zoom API requests.
        echo "Access Token: $accessToken";
    } else {
        // Handle the case where the access token was not obtained successfully
        echo "Failed to obtain access token";
    }
} else {
    // Handle the case where the authorization code is not present in the callback URL
    echo "Authorization code not found in callback URL.";
}
?>
