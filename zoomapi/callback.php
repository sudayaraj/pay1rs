<?php
error_reporting(E_ALL);
error_reporting(-1);
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
require 'dbconn.php';
require 'zoomconfig.php';
require 'vendor/autoload.php';

use League\OAuth2\Client\Provider\GenericProvider;


$clientId = ZOOM_CLIENT_ID;
$clientSecret = ZOOM_CLIENT_SECRET;
$redirectUri = ZOOM_CALLBACK;

$provider = new GenericProvider([
    'clientId' => $clientId,
    'clientSecret' => $clientSecret,
    'redirectUri' => $redirectUri,
    'urlAuthorize' => 'https://zoom.us/oauth/authorize',
    'urlAccessToken' => 'https://zoom.us/oauth/token',
    'urlResourceOwnerDetails' => 'https://api.zoom.us/v2/users/me',
    'scopes' => 'meeting:read meeting:write offline_access',
]);

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['error'])) {

        echo 'Error: ' . $_GET['error'];
    } elseif (isset($_GET['code'])) {

        $authorizationCode = $_GET['code'];


        $accessToken = $provider->getAccessToken('authorization_code', [
            'code' => $authorizationCode,
        ]);

        echo $sql = "INSERT INTO zoomapi (accesstoken) VALUES ('" . $accessToken->getToken() . "');";
        $result = $conn->query($sql);
        // echo 'Access Token: ' . $accessToken->getToken();
        $conn->close();
        if ($result == true) {
            redirect('meetinglink.php', 'refresh');
        }
    } else {
        echo 'Invalid request';
    }
} else {
    echo 'Invalid request method';
}
