<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('get_data_from_database')) {
    /**
     * Get data from the database based on a custom query.
     *
     * @param string $query SQL query
     * @return mixed Result set or FALSE on failure
     */
    function get_availableBalance($user_id)
    {
        $ci =& get_instance();
        $ci->load->database();

        $ci->db->where('userid', $user_id);
        $ci->db->order_by('created_datetime', 'desc');
        $ci->db->limit(1);
        $q = $ci->db->get('transaction_summary');
        if ($q->num_rows() > 0) {
            $row = $q->row_array();
            $data = $row['balance'];
        } else {
            $data = 0;
        }
        return $data;
    }
}
