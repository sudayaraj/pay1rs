<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Api_Model');
        $this->load->model('Imwallet_Model');

        header("Access-Control-Allow-Origin: http://localhost:8081");
        header("Access-Control-Allow-Origin: http://localhost:3000");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Authorization");
    }

    public function test()
    {
        echo 55;
    }
    public function login()
    {
        header("Access-Control-Allow-Origin: http://localhost:8081");
        header("Access-Control-Allow-Origin: http://localhost:3000");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Authorization");

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $post_data = json_decode($this->input->raw_input_stream);

            $username = strtolower($post_data->email);
            $password = $post_data->password;
            $validate_login = $this->Api_Model->validate_login($post_data);
            if ($validate_login) {
                $status = 'SUCCESS';
                $message = 'Signin successful';
                date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
                $timenow = date('d-m-Y H:i:s');
                $token = bin2hex(random_bytes(16));
                $this->db->where('id', $validate_login['id']);
                $this->db->update('users', array('last_login' => $timenow, 'token' => $token));
                $validate_login = $this->Api_Model->validate_login($post_data);
            } else {
                $status = 'FAILED';
                $message = 'Invalid Username or Password';
            }
            // log_message('error', 'POST data: ' . print_r($post_data, true));

            $response = ['message' => $message, 'status' => $status, 'data' => $validate_login];
        } else {
            $response = ['error' => 'Invalid request method'];
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function signup()
    {
        header("Access-Control-Allow-Origin: http://localhost:8081");
        header("Access-Control-Allow-Origin: http://localhost:3000");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Authorization");

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $post_data = json_decode($this->input->raw_input_stream);

            $email = $post_data->email;
            $password = $post_data->password;
            $name = $post_data->name;
            $dateOfBirth = $post_data->dateOfBirth;
            $mobile = $post_data->mobile;
            $validate_signup = $this->Api_Model->validate_signup($email);
            if (empty($validate_signup)) {
                $status = 'SUCCESS';
                $message = 'Signup Successful';
                date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
                $timenow = date('d-m-Y H:i:s');
                $token = bin2hex(random_bytes(16));

                $this->db->insert('users', array(
                    'email' => $email,
                    'password' => md5($password),
                    'name' => $name, 'dateOfBirth' => $dateOfBirth, 'mobile' => $mobile, 'last_login' => $timenow, 'token' => $token
                ));
                $data = ['name' => $name, 'email' => $email, 'dateOfBirth' => $dateOfBirth];
            } else {
                $status = 'FAILED';
                $message = 'Email already exists !';
                $data = '';
            }
            // log_message('error', 'POST data: ' . print_r($post_data, true));


            $response = ['message' => $message, 'status' => $status, 'data' => $data];
        } else {
            $response = ['error' => 'Invalid request method'];
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function getAvailableBalance()
    {

        header("Access-Control-Allow-Origin: http://localhost:3000");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Authorization");
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $get_data = json_decode($this->input->raw_input_stream);

            $tokenValidate = $this->Api_Model->tokenValidate($get_data);

            if ($tokenValidate) {
                $data = $this->Api_Model->get_walletbalance($get_data->userid);
                $status = 'SUCCESS';
                $message = $data;
            } else {
                $status = 'error';
                $message = 'Invalid Token';
            }

            $response = array(
                'status' => $status,
                'data' => $message
            );

            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response));
        } else {
            show_404();
        }
    }

    public function fetch_main_balance()
    {
        $this->load->model('Imwallet_Model');
        $response = $this->Imwallet_Model->fetch_imwallet_balance();

        // echo "<pre>";
        // print_r ($response);
        // echo "</pre>";

    }

    public function recharge_api()
    {

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $post_data = json_decode($this->input->raw_input_stream);
            $api_response = $this->Imwallet_Model->imwallet_recharge($post_data);

            $msg = $api_response->msg;
            $amount = $api_response->amount;
            $orderid = $api_response->orderid;
            $requestID = $api_response->requestID;
            $account = $api_response->account;
            $status = $api_response->status;

            $insert_response = [
                "msg" => $msg,
                "amount" => $amount,
                "orderid" => $orderid,
                "requestID" => $requestID,
                "account" => $account,
                "status" => $status
            ];
            // echo "<pre>";
            // print_r(json_decode($api_response));
            // echo "</pre>";
            // die;

            if ($status == 'SUCCESS') {
                $status = 'SUCCESS';
                $message = $msg;
                date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
                $timenow = date('d-m-Y H:i:s');
                $this->Imwallet_Model->payment_process($post_data->token, $post_data->plan);
                $this->db->insert('prepaid_recharge_api_response', $insert_response);

                $response = array(
                    'status' => $status,
                    'data' => $message
                );
            } else {
                $this->db->insert('prepaid_recharge_api_response', $insert_response);
                $status = 'FAILED';
                $message = $msg;
                $data = '';
            }
            // log_message('error', 'POST data: ' . print_r($post_data, true));


            $response = ['message' => $message, 'status' => $status, 'data' => $data];
        } else {
            $response = ['error' => 'Invalid request method'];
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    function recharge_callback()
    {
        // oprTID=214855791&orderid=1063438555ORD&account=9043684803&amount=97&skey=BS&status=SUCCESS	
        $status = 'SUCCESS';
        $message = json_encode($_GET);

        $response = array(
            'status' => $status,
            'data' => $message
        );

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function postpaidOperators()
    {
        $response = $this->Imwallet_Model->getOperator();


        $result = $response->data;
        foreach ($result as $results) {
            echo "<pre>";
            print_r($results);
            echo "</pre>";
            $insertData = [
                "getBill" => $results->getBill,
                "bbps_status" => $results->bbps_status,
                "service" => $results->service,
                "P2P_P2A" => $results->P2P_P2A,
                "name" => $results->name,
                "spkey" => $results->spkey,
                "state" => $results->state,
                "operator" => $results->operator,
            ];
            // $this->db->insert('operator_spkey', $insertData);

        }
    }

    public function postpaidFetchBill()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $post_data = json_decode($this->input->raw_input_stream);
            $postData_arr = [
                "operator" => $post_data->network,
                "mobile" => $post_data->mobile,
            ];
            $response = $this->Imwallet_Model->ppFetchBill($postData_arr);
            $api_response = $response->data;
            //             echo "<pre>";
            //             print_r ($response);
            //             echo "</pre>";
            // die;
            $status = $response->status;

            if ($status == 'SUCCESS') {
                if ($post_data->network == 'AT') {
                    $spkey = "29";
                }
                $status = 'SUCCESS';
                // $message = $msg;
                $billFetchId = $api_response->billFetchId;
                $billedamount = $api_response->billedamount;
                $billdate = $api_response->billdate;
                $payamount = $api_response->payamount;
                $customerName = $api_response->customerName;
                date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
                $timenow = date('d-m-Y H:i:s');

                $insert_response = [
                    "billFetchId" => $billFetchId,
                    "billedamount" => $billedamount,
                    "billdate" => $billdate,
                    "payamount" => $payamount,
                    "customerName" => $customerName,
                    "spkey" => $spkey,
                    "mobile"=>$post_data->mobile,
                    "status" => $status
                ];

                $this->db->insert('postpaid_fetchbill_api_response', $insert_response);
            } else {
                $msg = $response->msg;
                $insert_response = [
                    "msg" => $msg,
                    "status" => $status
                ];
                $this->db->insert('postpaid_fetchbill_api_response', $insert_response);
                $status = 'FAILED';
                $message = $msg;
                $data = '';
            }
            // log_message('error', 'POST data: ' . print_r($post_data, true));


            $response = ['status' => $status, 'data' => $insert_response];
        } else {
            $response = ['error' => 'Invalid request method'];
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function postpaidPayBill()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $post_data = json_decode($this->input->raw_input_stream);
            $billFetchId = $post_data->billFetchId;
            $this->db->where('billFetchId', $billFetchId);
            $q = $this->db->get('postpaid_fetchbill_api_response');
            $ppdetails = $q->row_array();
            $response = $this->Imwallet_Model->ppPayBill($ppdetails);
        } else {
            $response = ['error' => 'Invalid request method'];
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }
}
