<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Recharge extends CI_Controller
{

    public function fetch_balance()
    {
        $response = $this->api_getmethod();
        $result = json_decode($response);
        if ($result['status'] == 'SUCCESS') {
            echo $result['Bal'];
        } else {
            echo "API Error contact admin";
        }
    }

    public function callback(){
        $status = $_GET['status'];
        if($status=='PENDING'){
            $this->db->insert('imwallet_log', array('response'=>$_GET, 'status'=>2));
            echo "Please Wait your recharge will complete soon";            
        }else if($status=='SUCCESS'){
            $this->db->insert('imwallet_log', array('response'=>$_GET, 'status'=>1));
            echo "Recharge Successfully completed";
        }else{
            $this->db->insert('imwallet_log', array('response'=>$_GET, 'status'=>0));
            echo "Your recharge is failed, ".$_GET['msg'];
        }
    }

    public function api_getmethod()
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://partner.imwallet.in/web_services/checkBal.jsp?webToken=RhDCCxcXZAk6ZRDmPQDLUJZCdCCMOyMb&userCode=IMAPI5281341",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_COOKIE => "JSESSIONID=5F002F2EF9D4E61B45503406AEEEB689",
            CURLOPT_HTTPHEADER => [
                "User-Agent: insomnia/8.3.0"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
}

/* End of file Recharge.php */
