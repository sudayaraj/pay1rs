<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Imwallet extends CI_Controller
{

    public function fetch_imwallet_balance()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://partner.imwallet.in/web_services/checkBal.jsp?webToken=RhDCCxcXZAk6ZRDmPQDLUJZCdCCMOyMb&userCode=IMAPI5281341',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
}

/* End of file Imwallet.php */
