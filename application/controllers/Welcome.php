<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{
	public function index()
	{
		$this->load->view('home');
	}

	public function zoomindex()
	{
		$this->load->view('zoomapi/index');
	}

	public function callback()
	{
		$this->load->view('zoomapi/callback');
	}

	public function meetinglink(){
		$this->load->view('zoomapi/meetinglink');
	}

	public function zoomtest()
	{
		// Zoom OAuth 2.0 credentials
		$clientID = 'lZ59I5YNRJuohGDoo9L_Iw';
		$clientSecret = 'Q1N0vJdu6EdQXbkw8ZKQ5eqJdpgI7tCR';

		// Redirect URI where Zoom will send the authorization code
		$redirectURI = 'https://pay1rs.com/welcome/index';

		// Zoom API endpoint to request an access token
		$tokenURL = 'https://zoom.us/oauth/token';

		// Set the authorization code obtained after the user's approval
		$authorizationCode = 'AUTHORIZATION_CODE'; // Replace with the code you receive from the user's approval

		// Data to send to request an access token
		$data = [
			'grant_type' => 'authorization_code',
			'code' => $authorizationCode,
			'redirect_uri' => $redirectURI,
		];

		// Set up cURL to request an access token
		$ch = curl_init($tokenURL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Authorization: Basic ' . base64_encode($clientID . ':' . $clientSecret),
		]);

		$response = curl_exec($ch);
		curl_close($ch);

		if ($response) {
			$tokenData = json_decode($response);

			if (isset($tokenData->access_token)) {
				// Access token obtained
				$accessToken = $tokenData->access_token;

				// Zoom API endpoint for creating a meeting
				$createMeetingURL = 'https://api.zoom.us/v2/users/me/meetings';

				// Data to create a meeting
				$meetingData = [
					'topic' => 'My Public Meeting',
					'type' => 2, // 2 indicates a scheduled meeting
					'start_time' => '2023-11-01T15:00:00',
					'duration' => 60, // Meeting duration in minutes
					'settings' => [
						'join_before_host' => true,
						'registrants_email_notification' => false,
					],
				];

				// Set up cURL to create the meeting with the obtained access token
				$ch = curl_init($createMeetingURL);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($meetingData));
				curl_setopt($ch, CURLOPT_HTTPHEADER, [
					'Authorization: Bearer ' . $accessToken,
					'Content-Type: application/json',
				]);

				$response = curl_exec($ch);
				curl_close($ch);

				if ($response) {
					$result = json_decode($response);

					if (isset($result->join_url)) {
						$meetingLink = $result->join_url;
						echo 'Public Meeting Link: ' . $meetingLink;
					} else {
						echo 'Failed to create a meeting.';
					}
				}
			} else {
				echo 'Failed to obtain an access token.';
			}
		} else {
			echo 'Failed to make an API request for the access token.';
		}
	}
}
