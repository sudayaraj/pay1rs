<!DOCTYPE html>
<html>
<head>
    <title>Zoom OAuth</title>
</head>
<body>
<?php
$zoomClientId = 'o6k3xQ3nQ0WqWVn5cQe5NA'; // Replace with your Zoom App's client ID
$redirectUrl = 'https://pay1rs.com/welcome/callback'; // Replace with your actual callback URL

if (isset($_GET['code'])) {
    echo "Authorization Successful! You can close this window and check the callback.php page for the access token.";
} else {
    // Redirect the user to the Zoom authorization URL.
    $authorizationUrl = "https://zoom.us/oauth/authorize?response_type=code&client_id=$zoomClientId&redirect_uri=$redirectUrl";
    echo "<a href='$authorizationUrl'>Authorize with Zoom</a>";
}
?>
</body>
</html>
