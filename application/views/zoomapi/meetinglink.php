<?php
// Replace these with your OAuth app credentials
$clientId = 'o6k3xQ3nQ0WqWVn5cQe5NA';
$clientSecret = 'Br9CGUddhYT53m5q0zZMRNRZ6nlivyVM';

// Replace this with your redirect URI
$redirectUri = 'https://pay1rs.com/welcome/callback';

// Zoom API endpoints
$zoomApiBaseUrl = 'https://api.zoom.us/v2';
$zoomCreateMeetingEndpoint = '/users/me/meetings';

// Prepare the data for creating a meeting
$data = array(
    'topic' => 'Test Meeting',
    'type' => 2, // 2 for scheduled meeting
    'start_time' => '2023-11-01T14:00:00Z', // Replace with your desired start time
    'duration' => 60, // Duration in minutes
);

// Obtain an access token using OAuth 2.0 (you may use a library for this step)
// Make a POST request to https://zoom.us/oauth/token with your credentials and get an access token

$accessToken = 'eyJzdiI6IjAwMDAwMSIsImFsZyI6IkhTNTEyIiwidiI6IjIuMCIsImtpZCI6IjkyOTgwNWZjLTcyNWYtNDViYS1iZDVhLTEzM2U2NWU5YjE3NiJ9.eyJ2ZXIiOjksImF1aWQiOiJlZjNmYjMwOGNhYzVkYjliZTYwYTI1NDU1MTkzNmU2MCIsImNvZGUiOiJqRFFsZWJHaFJsUVdwT2JUSGRvVDNTeTIzQ1lRVE5hVnciLCJpc3MiOiJ6bTpjaWQ6bzZrM3hRM25RMFdxV1ZuNWNRZTVOQSIsImdubyI6MCwidHlwZSI6MCwidGlkIjowLCJhdWQiOiJodHRwczovL29hdXRoLnpvb20udXMiLCJ1aWQiOiJrdFlsc3B4TlN4Q2ljZ21Qck93TllnIiwibmJmIjoxNjk5MDA0NTc2LCJleHAiOjE2OTkwMDgxNzYsImlhdCI6MTY5OTAwNDU3NiwiYWlkIjoidDQ0OW9GVXhRdGlnOGlFYTZlVnhWZyJ9.Z3c7SckjDoibdjAWEvwZw_OYhebnKbfphaVmA5npLHiUCseFB-42H3lTdnoONvA9h9E-sWJQRqgM4jpwf0MNKw'; // Replace with the access token you obtained

// Create the cURL request to create a meeting
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $zoomApiBaseUrl . $zoomCreateMeetingEndpoint);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: Bearer ' . $accessToken,
    'Content-Type: application/json',
));
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

$response = curl_exec($ch);

if (curl_errno($ch)) {
    echo 'Error: ' . curl_error($ch);
} else {
    $responseData = json_decode($response, true);

    if (isset($responseData['join_url'])) {
        echo 'Meeting link: <a href="' . $responseData['join_url'].'"> Join Now </a>';
    } else {
        echo 'Error creating meeting: ' . $responseData['message'];
    }
}

curl_close($ch);
?>
