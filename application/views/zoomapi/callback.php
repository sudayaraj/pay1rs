<?php
require '../../../zoomapi/zoomconfig.php';
// require 'zoomconfig.php';
require '../../../zoomapi/vendor/autoload.php'; // Include the OAuth2 client library

use League\OAuth2\Client\Provider\GenericProvider;

// Replace with your Zoom OAuth app credentials
$clientId = ZOOM_CLIENT_ID;
$clientSecret = ZOOM_CLIENT_SECRET;
$redirectUri = ZOOM_CALLBACK;

$provider = new GenericProvider([
    'clientId' => $clientId,
    'clientSecret' => $clientSecret,
    'redirectUri' => $redirectUri,
    'urlAuthorize' => 'https://zoom.us/oauth/authorize',
    'urlAccessToken' => 'https://zoom.us/oauth/token',
    'urlResourceOwnerDetails' => 'https://api.zoom.us/v2/users/me', // Adjust based on Zoom's API
    'scopes' => 'meeting:read meeting:write', // Specify your required scopes
]);

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['error'])) {
        // Handle errors, if any
        echo 'Error: ' . $_GET['error'];
    } elseif (isset($_GET['code'])) {
        // Extract the authorization code from the URL
        $authorizationCode = $_GET['code'];

        // Exchange the authorization code for an access token
        $accessToken = $provider->getAccessToken('authorization_code', [
            'code' => $authorizationCode,
        ]);

        // Use the obtained access token to interact with the Zoom API
        // For example, you can make API requests here.

        // To test, you can output the access token:
        echo 'Access Token: ' . $accessToken->getToken();
    } else {
        echo 'Invalid request';
    }
} else {
    echo 'Invalid request method';
}
