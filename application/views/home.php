<?php $this->load->view('header'); ?>
<style>
    .nav-brand,
    .nav-link {
        color: white !important;
    }


    .card {
        position: relative;
        overflow: hidden;
        transition: transform 0.5s;
    }

    .card:hover::before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: linear-gradient(to right, rgba(255, 255, 255, 0.5), transparent, rgba(255, 255, 255, 0.5));
        transform: rotate(180deg);
        opacity: 0;
        transition: opacity 0.5s;
    }

    .card:hover {
        transform: scale(1.05);
        /* Add scaling on hover */
    }

    .card:hover::before {
        opacity: 1;
        /* Fade in the shine effect on hover */
    }

    /* //99 */
    .style__postContainer {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 164px;
        height: 32px;
        background: #ffffff;
        border: 1px solid #e5f3fd;
        border-radius: 8px;
        cursor: pointer;
    }

    .style__post {
        font-weight: 600;
        font-size: 14px;
        line-height: 20px;
        color: #041533;
        margin-right: 6px;
        margin-top: -2px;
    }

    .style__shimmer {
        display: flex;
        justify-content: center;
        align-items: center;
        background: #219653;
        border: 1px solid #d7f2e3;
        border-radius: 4px;
        -webkit-mask: linear-gradient(-90deg, #000 60%, rgba(0, 0, 0, 0.333333) 65%, #000 70%) right/310% 90%;
        background-repeat: no-repeat;
        animation: style__shimmer 2.5s infinite;
        animation-iteration-count: 2 !important;
    }

    .style__free {
        font-weight: 700;
        line-height: 9px;
        letter-spacing: 0.02em;
        text-transform: uppercase;
        color: #ffffff;
    }
</style>

<body>
    <nav class="navbar navbar-expand-lg bg-body-tertiary" style="background-color: #ff004a !important;">
        <div class="container-fluid">
            <a class="navbar-brand" style="color: white !important;" href="#">Pay1rs</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li> -->

                </ul>
                <form class="d-flex" role="search">
                    <div class="style__postContainer">
                        <div class="style__post">Post property</div>
                        <div style="width:36px;height:15px" class="style__shimmer">
                            <div class="style__shimmer" style="width: 36px; height: 15px;">
                                <div class="style__free" style="font-size: 10px;">FREE</div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row mt-3">
            <div class="col-md-3 mr-3">
                <div class="card">
                    <img src="<?= base_url() ?>assets/img/girl_hold_house.jpg" protected class="card-img-top img-fluid" alt="...">
                    <div class="card-img-overlay">
                        <h5 class="card-title">Recent Ads</h5>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mr-3">
                <div class="card">
                    <img src="<?= base_url() ?>assets/img/friends_trip.jpg" protected class="card-img-top img-fluid" alt="...">
                    <div class="card-img-overlay">
                        <h5 class="card-title">Travel Tips</h5>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mr-3">
                <div class="card">
                    <img src="<?= base_url() ?>assets/img/girl_eating_popcorn.jpg" protected class="card-img-top img-fluid" alt="...">
                    <div class="card-img-overlay">
                        <h5 class="card-title" style="color: white;text-align:right;">Ott Updates</h5>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mr-3">
                <div class="card">
                    <img src="<?= base_url() ?>assets/img/rating_star_movies.jpg" protected class="card-img-top img-fluid" alt="...">
                    <div class="card-img-overlay">
                        <h5 class="card-title" style="text-align:right;">Ratings / Reviews</h5>
                        <!-- <p class="card-text" style="text-align:right;">Movie</p> -->
                    </div>
                </div>
            </div>
        </div>

        
    </div>

</body>
<?php $this->load->view('footer'); ?>