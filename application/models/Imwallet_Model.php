<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Imwallet_Model extends CI_Model
{
    public function fetch_imwallet_balance()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://partner.imwallet.in/web_services/checkBal.jsp?webToken=RhDCCxcXZAk6ZRDmPQDLUJZCdCCMOyMb&userCode=IMAPI5281341',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }

    public function imwallet_recharge($post_data)
    {
        $available_balance = $this->get_availableBalance($post_data->id);
        if ($available_balance >= $post_data->plan) {


            $url = "webToken=RhDCCxcXZAk6ZRDmPQDLUJZCdCCMOyMb&userCode=IMAPI5281341&orderid=" . rand() . "&skey=" . $post_data->network . "&accountNo=" . $post_data->mobile . "&amount=" . $post_data->plan . "&callBack=https://pay1rs.com/api/recharge_callback";
            file_put_contents('./dump/recharge_RQ_' . date("j.n.Y") . '.log', $url, FILE_APPEND);

            $insert_request = [
                "get_url" => 'https://partner.imwallet.in/web_services/recharge_process.jsp?' . $url,
            ];
            $this->db->insert('prepaid_recharge_api_request', $insert_request);


            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://partner.imwallet.in/web_services/recharge_process.jsp?' . $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            file_put_contents('./dump/recharge_RS_' . date("j.n.Y") . '.json', $response, FILE_APPEND);
            curl_close($curl);            
            return $response;
        }else{
            $response = [
                "msg" => "Please Top-up your wallet and try again",
                "amount" => $post_data->plan,
                "orderid" => rand(),
                "requestID" => rand(),
                "account" => $post_data->mobile,
                "status" => "FAILED"
            ];
            return json_encode($response);
        }
    }

    public function payment_process($token, $plan_amt)
    {
        $this->db->where('token', $token);
        $user = $this->db->get('users');
        if (!empty($user)) {
            $userDetails = $user->row_array();
            $user_id = $userDetails['id'];
            $transactionID = $this->generateTransactionID('Recharge_TXT');
            $trans_date = date("Y-m-d");
            $available_balance = $this->get_availableBalance($user_id);
            if ($available_balance >= $plan_amt) {
                $current_balance = ($available_balance - $plan_amt);
                $debit = $plan_amt;
                $insert_data = [
                    "userid" => $user_id,
                    "transactionid" => $transactionID,
                    "trans_date" => $trans_date,
                    "debit" => $debit,
                    "balance" => $current_balance,
                ];
                $this->db->insert('transaction_summary', $insert_data);
            }
        }
    }

    function generateTransactionID($prefix = '')
    {
        // Generate a timestamp
        $timestamp = date("YmdHis");

        // Generate a random component (you can customize the length as needed)
        $random = mt_rand(1000, 9999);

        // Concatenate the prefix, timestamp, and random component
        $transactionID = $prefix . $timestamp . $random;

        return $transactionID;
    }

    // public function bbps_recharge()
    // {

    //     $url = "webToken=&userCode=&orderid=" . rand() . "&skey=" . $post_data->network . "&accountNo=" . $post_data->mobile . "&amount=" . $post_data->plan . "&callBack=https://pay1rs.com/api/recharge_callback";

    //     $arrayVar = [
    //         "webToken" => "RhDCCxcXZAk6ZRDmPQDLUJZCdCCMOyMb",
    //         "userCode" => "IMAPI5281341",
    //         "parameters" => [
    //             "account" => "",
    //             "spkey" => "",
    //             "cust_mbl" => "",
    //             "ad1" => "",
    //             "ad2" => "",
    //             "ad3" => "",
    //         ],
    //     ];
    //     file_put_contents('./dump/recharge_RQ_' . date("j.n.Y") . '.log', $url, FILE_APPEND);

    //     $insert_request = [
    //         "get_url" => 'https://partner.imwallet.in/web_services/recharge_process.jsp?' . $url,
    //     ];
    //     $this->db->insert('prepaid_recharge_api_request', $insert_request);

    //     $curl = curl_init();

    //     curl_setopt_array($curl, [
    //         CURLOPT_URL => "https://partner.imwallet.in/web_services/BBPS/getOperator.jsp",
    //         CURLOPT_RETURNTRANSFER => true,
    //         CURLOPT_ENCODING => "",
    //         CURLOPT_MAXREDIRS => 10,
    //         CURLOPT_TIMEOUT => 30,
    //         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //         CURLOPT_CUSTOMREQUEST => "POST",
    //         CURLOPT_POSTFIELDS => "{\n  \"webToken\": \"Your Web Token\",\n  \"userCode\": \"USER ID\",\n  \"parameters\": {\n    \"category\": \"Electricity\"\n  }\n}",
    //         CURLOPT_HTTPHEADER => [
    //             "Accept: */*",
    //             "Content-Type: application/json"
    //         ],
    //     ]);

    //     $response = curl_exec($curl);
    //     $err = curl_error($curl);

    //     // if ($err) {
    //     //     echo "cURL Error #:" . $err;
    //     // } else {
    //     //     echo $response;
    //     // }

    //     file_put_contents('./dump/recharge_RS_' . date("j.n.Y") . '.json', $response, FILE_APPEND);
    //     curl_close($curl);
    //     return $response;
    // }

    public function getOperator()
    {
        $postData = [
            "webToken" => "RhDCCxcXZAk6ZRDmPQDLUJZCdCCMOyMb",
            "userCode" => "IMAPI5281341",
            "parameters" => ["category" => "Postpaid"],
        ];

        $url = "https://partner.imwallet.in/web_services/BBPS/getOperator.jsp";

        return $this->apiCall($url, $postData);
    }

    public function ppFetchBill($param)
    {
        if ($param['operator'] == 'AT') {
            $spkey = "29";
        }
        $postData = [
            "webToken" => "RhDCCxcXZAk6ZRDmPQDLUJZCdCCMOyMb",
            "userCode" => "IMAPI5281341",
            "parameters" => ["account" => $param['mobile'], "spkey" => $spkey, "cust_mbl" => $param['mobile'], "ad1" => "", "ad2" => "", "ad3" => ""],
        ];

        $url = "https://partner.imwallet.in/web_services/BBPS/fetchBill.jsp";

        return $this->apiCall($url, $postData, "ppFetchBill");
    }

    public function ppPayBill($param)
    {

        $postData = [
            "webToken" => "RhDCCxcXZAk6ZRDmPQDLUJZCdCCMOyMb",
            "userCode" => "IMAPI5281341",
            "parameters" => ["account" => $param['mobile'], "spkey" => $param['spkey'], "amount" => $param['payamount'], "orderid" => "'" . rand() . "'", "billFetchId" => $param['billFetchId'], "callBack" => "https://pay1rs.com/api/recharge_callback", "ad1" => "", "ad2" => "", "ad3" => "", "cust_mbl" => $param['mobile']],
        ];

        $url = "https://partner.imwallet.in/web_services/BBPS/payBill.jsp";

        return $this->apiCall($url, $postData, "ppPayBill");
    }

    public function apiCall($url, $postData, $type)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'data=' . json_encode($postData),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
            ),
        ));

        $response = curl_exec($curl);
        $this->log_dump($response, $type . "_RS_");
        $this->log_dump(json_encode($postData), $type . "_RQ_");
        curl_close($curl);
        return json_decode($response);
    }

    public function log_dump($response, $type)
    {
        if (!file_exists('./dump/')) {
            mkdir('./dump/', 0777, true);
        }
        file_put_contents('./dump/' . $type . '' . date("j.n.Y") . '.json', $response, FILE_APPEND);
    }

    public function get_availableBalance($userid)
    {
        $this->db->where('userid', $userid);
        $this->db->order_by('created_datetime', 'desc');
        $this->db->limit(1);
        $q = $this->db->get('transaction_summary');
        if ($q->num_rows() > 0) {
            $row = $q->row_array();
            $data = $row['balance'];
        } else {
            $data = 0;
        }
        return $data;
    }

}
