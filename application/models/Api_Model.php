<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api_Model extends CI_Model
{
    public function validate_login($post_data)
    {
        $username = $post_data->email;
        $password = $post_data->password;
        $this->db->select('id,mobile,name,email,user_type,last_login,token');

        $this->db->where(array("email" => $username, "password" => md5($password), "isActive" => 1));
        $q = $this->db->get('users');
        if ($q->num_rows() > 0) {
            return $q->row_array();
        } else {
            return false;
        }
    }

    public function validate_signup($email)
    {   
        $this->db->select('*');
        $this->db->where(array("email" => $email));
        $q = $this->db->get('users');
        if ($q->num_rows() > 0) {
            return $q->row_array();
        } else {
            return false;
        }
    }




    public function tokenValidate($get_data)
    {
        $user_id = $get_data->userid;
        $token = $get_data->token;
        $this->db->where(array("id" => $user_id, "token" => $token, "isActive" => 1));
        $q = $this->db->get('users');
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function get_walletbalance($userid)
    {
        $this->db->where('userid', $userid);
        $this->db->order_by('created_datetime', 'desc');
        $this->db->limit(1);
        $q = $this->db->get('transaction_summary');
        if ($q->num_rows() > 0) {
            $row = $q->row_array();
            $data = $row['balance'];
        } else {
            $data = 0;
        }
        return $data;
    }
}
